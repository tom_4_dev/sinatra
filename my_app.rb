# myapp.rb
require 'sinatra'
#require 'aws_record'
require 'active_record'
require 'sinatra/activerecord'
#require 'sinatra/activerecord/rake'
require 'pg'

configure :development do
  set :database, {adapter: 'postgresql',  encoding: 'unicode', database: 'test_database', pool: 2, username: 'pguser', password: 'psql', host: 'localhost'}
end

class User < ActiveRecord::Base
end

get '/api/v1/users' do
  content_type :json
  #{ first_name: 'Santanu', last_name: "Bhattacharya" }.to_json
  User.all.to_json
end
