
# required ruby version
1. ruby 2.5 (rvm install 2.5.0)

# required gems
1. gem install sinatra
2. gem install jets
3. gem install pg
4. gem install active_record
5. gem install sinatra-active-record
